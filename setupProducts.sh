#unsetup_all
export PROJECT_HOME=$(pwd)
export PATH=$PATH:${PROJECT_HOME}/bin
source /cvmfs/fermilab.opensciencegrid.org/products/artdaq/setup
#setup gcc v9_3_0
setup cmake v3_20_0
#setup root v6_22_06b -q e20:p392:prof
#setup python v3_9_2
#ln -s venv/lib64/python3.6/site-packages/qt5_applications/Qt/bin/designer .
