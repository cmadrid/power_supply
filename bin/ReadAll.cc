/*!
 * \authors Mattia Lizzo <mattia.lizzo@cern.ch>, INFN-Firenze
 * \authors Francesco Fiori <francesco.fiori@cern.ch>, INFN-Firenze
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date Sep 2 2019
 */

// Libraries
#include "DeviceHandler.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include <boost/program_options.hpp> //!For command line arg parsing
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <sys/time.h>
#include <thread>         // std::thread
#include <chrono>

//https://github.com/gipert/progressbar
#include "progressbar.hpp"

#define DEFAULT_MARKER_STYLE 20
#define DEFAULT_MARKER_SIZE 0.3
#define TIME_FORMAT "%H:%M:%S %m-%d-%Y"

std::map<std::string, PowerSupplyChannel*> channelInfoMap_;

// Namespaces
using namespace std;
namespace po = boost::program_options;

/*!
************************************************
* Helper functions
************************************************
*/
std::string split(const std::string& half, const std::string& s, const std::string& h)
{
    std::string token;
    if      ("first"==half) token = s.substr(0, s.find(h));
    else if ("last" ==half) token = s.substr(s.find(h) + h.length(), std::string::npos);
    return token;
} 

template<typename T> bool sortByChannelNumber(const std::pair<std::string,T*>& c1, const std::pair<std::string,T*>& c2)
{
  const auto& num1 = std::stoi(split("last", c1.first, "_"));
  const auto& num2 = std::stoi(split("last", c2.first, "_"));
  return ( num1 < num2 );
}

template<typename T> std::vector<std::pair<std::string, T*>> getStringSortedVector(std::map<std::string, T*>& map)
{
    std::vector<std::pair<std::string, T*>> vec(map.begin(), map.end());
    std::sort(vec.begin(), vec.end(), sortByChannelNumber<PowerSupplyChannel>);
    return vec;
}

/*!
************************************************
* Read data from power supply.
************************************************
*/
const std::string readAllCurrents(void)
{
    const auto& channelInfoVec = getStringSortedVector(channelInfoMap_);

    struct timeval currentTime;
    gettimeofday(&currentTime, nullptr);
    long long time = currentTime.tv_sec; 

    std::string data = "time, " + std::to_string(time);
    for(auto& channel: channelInfoVec)
    {
        std::string channelName = channel.first;
        //<< " VoltageSet: " << channel.second->getVoltageCompliance() 
        data += ", V_" + channelName + ", " + std::to_string(channel.second->getOutputVoltage());
	data += ", I_" + channelName + ", " + std::to_string(channel.second->getCurrent());
    }
    return data;
}

/*!
************************************************
* Argument parser.
************************************************
*/
po::variables_map process_program_options(const int argc, const char* const argv[])
{
    po::options_description desc("Allowed options");

    desc.add_options()
      ("help,h", "produce help message")
      ("config,c",  po::value<string>()->default_value("default"), "set configuration file path (default files defined for each test) " "...")
      ("verbose,v", po::value<string>()->implicit_value("0"),      "verbosity level")
      ("delay,d",   po::value<double>()->default_value(1.1),       "define time between CAEN reads")
      ("nReads,n",  po::value<int>()->default_value(5),            "define number of CAEN reads");
    
    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
    }
    catch(po::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    po::notify(vm);

    // Help
    if(vm.count("help"))
    {
        cout << desc << "\n";
        return 0;
    }

    // Power supply object option
    if(vm.count("object")) { cout << "Object to initialize set to " << vm["object"].as<string>() << endl; }

    // Test type execution
    // if(vm.count("test")) test(vm["test"].as<string>(), vm["config"].as<string>(), vm.count("verbose"));
    return vm;
}

/*!
 ************************************************
 * Main.
 ************************************************
 */
int main(int argc, char* argv[])
{
    boost::program_options::variables_map v_map = process_program_options(argc, argv);
    std::cout << "Initializing ..." << std::endl;

    std::string docPath = v_map["config"].as<string>();
    pugi::xml_document docSettings;
    DeviceHandler theHandler;
    theHandler.readSettings(docPath, docSettings);
    pugi::xml_node fDocumentRoot = docSettings.child("Devices");
    std::map<std::string, std::map<std::string, PowerSupplyChannel*>> channelMap;

    for(pugi::xml_node powerSupply = fDocumentRoot.child("PowerSupply"); powerSupply; powerSupply = powerSupply.next_sibling("PowerSupply"))
    {
        std::string inUse = powerSupply.attribute("InUse").value();
        if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;
        std::string supplyID = powerSupply.attribute("ID").value();

        for(pugi::xml_node channel = powerSupply.child("Channel"); channel; channel = channel.next_sibling("Channel"))
        {
            std::string inUse = channel.attribute("InUse").value();
            if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;
            std::string channelID = channel.attribute("ID").value();

            try
            {
                PowerSupply* pSSupply           = theHandler.getPowerSupply(supplyID);
                channelMap[supplyID][channelID] = pSSupply->getChannel(channelID);
                channelInfoMap_[channelID] = pSSupply->getChannel(channelID);
            }
            catch(const std::out_of_range& oor)
            {
                std::cerr << "Out of Range error: " << oor.what() << '\n';
                throw std::out_of_range(oor.what());
            }
        }
    }

    // Get time for log file
    struct timeval currentTime;
    gettimeofday(&currentTime, nullptr);
    long long time = currentTime.tv_sec; 

    // Define log file and variables needed to read data
    //std::string path = "/home/daq/SurvivalBeam2021/JARVIS/SlowControl/";
    std::string path = "/home/daq/power_supply/build";
    std::string logName = path+"/CAENLogs/CAEN_HV_LOGS_"+std::to_string(time)+".txt";
    std::ofstream outFile(logName);
    int nReads = v_map["nReads"].as<int>();
    double delay = v_map["delay"].as<double>();

    // Loop to read and save data
    std::cout << "------------------------------------------------------------------------" << std::endl;
    std::cout << "Taking "<<nReads<<" total reads with a daylay of "<<delay<<" seconds" << std::endl;
    std::cout << "Will finish in ~"<<nReads*delay<<" seconds ("<<nReads*delay/(60.0*60.0)<<"hrs)" << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl;
    std::cout << "Starting logging ..." << std::endl;

    progressbar bar(nReads);
    bar.set_done_char("█");
    for(int i = 0; i < nReads; i++)
    {
	bar.update();
        const auto& data = readAllCurrents();
	//std::cout << "------------------------------------------------------------------------" << std::endl;
	//std::cout << data << std::endl;
	outFile << data << std::endl;

	std::ofstream overwriteFile(path+"/CAEN_HV_active.txt", std::ofstream::trunc);
	overwriteFile << data << std::endl;
	overwriteFile.close();

	if(i != nReads-1) std::this_thread::sleep_for( std::chrono::duration<double>(delay) );
	else std::cout<<std::endl;
    }
    std::cout << "Finished logging" << std::endl;
    outFile.close();

    return 0;
}
